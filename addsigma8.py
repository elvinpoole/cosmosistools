import sys 
import numpy as np
from cosmosis.runtime.config import Inifile
from cosmosis.runtime.pipeline import LikelihoodPipeline
from cosmosis.runtime.parameter import Parameter

args = sys.argv
assert len(args) == 2, 'wrong number of args'
chain_file = sys.argv[1]
print 'adding sigma_8 to file: {0}'.format(chain_file)

s = open(chain_file,'r').read()
lines = s.split('\n')
lines = np.array([line for line in lines if len(line) > 0]) #remove empty lines

end_of_header_index = np.where(np.array(lines) == '## END_OF_PRIORS_INI')[0][0]

#check to see if this is a multinest chain
matches = np.where( np.array([line[:9] for line in lines]) == '#nsample=' )[0]
if len(matches) == 1:
	print 'This is a multinest chain, will include the footer'
	start_of_footer_index = matches[0]
	footer = '\n'.join(lines[start_of_footer_index:])
elif len(matches) == 0:
	print 'This is not a multinest chain, assuming there is no footer'
	footer = ''
else:
	raise IOError('There is something weird about your chain')

first_header_line = lines[0]
extra_header = '\n'.join(lines[1:end_of_header_index+1])
extra_header = extra_header + '\n# Column COSMOLOGICAL_PARAMETERSSIGMA_8 added by python tool addsigma8 from cosmosistools'

colnames = first_header_line.split('\t')
colnames[0] = colnames[0][1:]

data = np.loadtxt(chain_file, unpack=True)

parameters = []
values_dict = {}
cosmo_data = []
for icol, colname in enumerate(colnames):
	if 'cosmological_parameters' in colname:
		name = colname.split('--')[-1]
		p = Parameter('cosmological_parameters', name, 0.5, limits=(0.0,1.0), prior=None)
		parameters.append(p)
		values_dict['cosmological_parameters', name] = '0.0 0.5 1.0'
		print 'cosmological_parameters', name
		col = data[icol]
		cosmo_data.append(col)
	else:
		'fixing systematic param {0} to 0'.format(colname)
		section = colname.split('--')[0]
		name = colname.split('--')[-1]
		values_dict[section, name] = '0.0'

cosmo_data = np.array(cosmo_data)

params_dict = {
	('runtime','sampler'):'test',

	('test','save_dir'):'test_output',

	('pipeline','modules'):'consistency camb',
	('pipeline','values'):'values.ini',

	('consistency','file'):'cosmosis-standard-library/utility/consistency/consistency_interface.py',

	('camb','file'):'cosmosis-standard-library/boltzmann/camb/camb.so',
	('camb','mode'):'all',
	('camb','lmax'):'5',
	('camb','feedback'):'0',
	('camb','kmin'):'1.0',
	('camb','kmax'):'10.0',
	('camb','nk'):'10',
	('camb','zmin'):'0.0',
	('camb','zmax'):'1.0',
	('camb','nz'):'2',
	('camb','accuracy_boost'):'0.3',
}

#Background, thermal, cmb, or all

ini = Inifile(None,override=params_dict)
pipeline = LikelihoodPipeline(ini, override=values_dict)

pipeline.timing = True
#pipeline = LikelihoodPipeline( ini )

sig8_col = []
for line in np.transpose(cosmo_data):
	pipe_data = pipeline.run_parameters(line)
	sig8 = pipe_data['cosmological_parameters','sigma_8']
	sig8_col.append(sig8)
	break



#might not need to do this
#pipeline.parameters = parameters
#pipeline.reset_fixed_varied_parameters()
#pipeline.print_priors()
#pipeline.setup()

















