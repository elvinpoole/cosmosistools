import lsssys.fileformat
import sys


def main():

	print sys.argv
	if len(sys.argv) != 4:
		raise IOError('Wrong number of inputs\nusage:\nbf2values.py best_fit.txt old_values.ini new_values.ini\nor\nbf2values    best_fit.txt old_values.ini new_values.ini')
	
	bf_file = sys.argv[1]
	valuesfile = sys.argv[2]
	bf_out   = sys.argv[3]

	lsssys.cosmosis_bf2values(bf_out, bf_file , valuesfile)

	return

if __name__ == '__main__':
	main()