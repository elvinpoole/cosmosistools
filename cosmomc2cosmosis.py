import sys
import os
import numpy as np 

def omb(chain):
	return chain['omegabh2']/((chain['H0']/100.)**2.)

def h(chain):
	return chain['H0']/100.

def a_s(chain):
	return chain['A']/(10.**9.)

def massless_nu(chain):
	return chain['nnu']-3.

def delta_neff(chain):
	return chain['nnu']-3.046

params = [
	('cosmological_parameters--omega_m','omegam'),
	('cosmological_parameters--h0',h),
	('cosmological_parameters--omega_b',omb),	
	('cosmological_parameters--n_s','ns'),
	('cosmological_parameters--a_s',a_s),	
	('cosmological_parameters--omnuh2','omeganuh2'),
	('cosmological_parameters--massless_nu',massless_nu),
	('cosmological_parameters--delta_neff',delta_neff),
	('cosmological_parameters--meffsterile','meffsterile'),
	('COSMOLOGICAL_PARAMETERSSIGMA_8','sigma8'),
	('COSMOLOGICAL_PARAMETERSS8','S8'),
	('weight','weight'),
	('post','like'),
]


def cosmomc2cosmosis(indir, infile_base, outfile, nfiles, params, outfile_full = None, extraparams=None, thinsamples=None):
	"""
	Convert the cosmomc output (e.g. planck chain) to a format that can be read by cosmosis
	If you want to use it for importance sampling with cosmosis, provide params and outfile2

	indir				location of the cosmomc files
	infile_base			name of the cosmomc run (e.g. base_plikHM_TT_lowTEB)
	nfiles = 1,			number of cosmomc chain files
	outfile    			optional: name of the output cosmosis file taking only the columns in params
	outfile2 - None 			name of the output cosmosis file taking all columns from planck chain
	params 				optional: parameters to be included in cosmosis file [('cosmosis_param_name1','cosmomc_param_name1'), ... ] 
	outfile = None,    	optional: name of the output cosmosis file taking only the columns in params
	extraparams=None,   optional: extra params to be sampled over randomly with uniform prior {'cosmosis_param_name':[xmin,xmax]}
	thinsamples=None	optional: thin the chain down to this many samples
	"""
	outfile_full = outfile+'.allcolumns'
	cmcfiles = [f for f in os.listdir(indir) if infile_base in f]
	#chain_files = [f for f in cmcfiles if infile_base+'_' in f]
	chain_files = [f for f in cmcfiles if f[:len(infile_base)] == infile_base and f[len(infile_base)+2:] == '.txt']
	assert len(chain_files) == nfiles

	for ifile, chain_file in enumerate(chain_files):
		if ifile == 0:
			data = np.loadtxt(indir + chain_file, unpack = True)
		else:
			data = np.hstack((data,  np.loadtxt(indir + chain_file, unpack = True)))

	if thinsamples is not None:
		n = len(data[0])
		assert thinsamples < n
		select = np.random.choice(thinsamples*[True]+(n-thinsamples)*[False], size=n, replace=False)
		data = data[:,select]

	paramsfilename = indir + infile_base + '.paramnames'
	paramsfile = open(paramsfilename)
	params1 = ['weight', 'like'] + [l.split('\t')[0] for l in paramsfile.readlines()]

	assert data.shape[0] == len(params1)

	#if outfile_full is specifed also make a file with all cosmomc columns
	header = '\t'.join(params1) + '\n' + 'sampler=weighted_metropolis' + '\n' + 'generated with cosmomc'

	np.savetxt(outfile_full,  np.transpose(data), header = header)
	#if nfiles > 1:
	#	filelength = (len(data[0])/nfiles)+1
	#	for i in xrange(nfiles):
	#		np.savetxt(outfile_full+'.{0}'.format(i),  np.transpose(data)[i * filelength: (i+1)*filelength], header = header)


	assert outfile is not None
	data2 = np.genfromtxt(outfile_full, names = True)
	#cosmosis_paramnames = params.keys()

	data3 = [] 
	params2 = []

	#for cosmosis_param in cosmosis_paramnames:
	#params_keys = params.keys()
	for iparam in xrange(len(params)):
		cosmosis_paramname = params[iparam][0]
		cosmomc_paramname  = params[iparam][1]

		if isinstance(cosmomc_paramname, str) == True:
			try:
				data3.append(data2[cosmomc_paramname])
			except ValueError:
				print 'parameter {cosmomc_paramname} not found in cosmomc file, skipping this parameter'.format(cosmomc_paramname=cosmomc_paramname)
				continue
		else: #it will be a function
			try:
				data3.append(cosmomc_paramname(data2))
			except ValueError:
				print 'parameter {cosmomc_paramname} not found in cosmomc file, skipping this parameter'.format(cosmomc_paramname=cosmomc_paramname)
				continue
		params2.append(cosmosis_paramname)

	if extraparams is not None:
		#generate random values for the extra params (e.g nuisance params)
		n = len(data3[0])
		for extraparam in extraparams.keys():
			r = extraparams[extraparam][0] + np.random.rand(n)*(extraparams[extraparam][1]-extraparams[extraparam][0])
			data3.append(r)
			params2.append(extraparam)

	header2 = '\t'.join(params2) + '\n' + 'sampler=weighted_metropolis' + '\n' + 'generated with cosmomc'
	np.savetxt(outfile,  np.transpose(data3), header = header2)
	#if nfiles > 1:
	#	filelength = (len(data3[0])/nfiles)+1
	#	for i in xrange(nfiles):
	#		np.savetxt(outfile+'.{0}'.format(i),  np.transpose(data3)[i * filelength: (i+1)*filelength], header = header2)

	return


def main():

	#print sys.argv
	if len(sys.argv) != 4:
		raise IOError('Wrong number of inputs\nusage:\ncosmomc2cosmosis cosmomc_filename(no numbers) <number of cosmomc files> cosmosis_filename')
	
	cosmomc_label = sys.argv[1]
	nfiles = int(sys.argv[2])
	cosmosis_output = sys.argv[3]

	#if cosmomc filename has a "/" in it, take the working directory from this
	if '/' in cosmomc_label:
		indir = '/'.join(cosmomc_label.split('/')[:-1]) + '/'
		cosmomc_label = cosmomc_label.split('/')[-1]
	#else use cwd
	else:
		indir = os.getcwd()+'/'

	cosmomc2cosmosis(indir, cosmomc_label, cosmosis_output, params = params, nfiles=nfiles)

	return

if __name__ == '__main__':
	main()

