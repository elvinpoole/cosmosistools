import lsssys.fileformat
import sys

def main():

	print sys.argv
	if len(sys.argv) != 4:
		raise IOError('Wrong number of inputs\nusage:\nmeans2values.py means.txt old_values.ini new_values.ini\nor\nmeans2values    means.txt old_values.ini new_values.ini')
	
	means_file = sys.argv[1]
	valuesfile = sys.argv[2]
	mean_out   = sys.argv[3]

	lsssys.fileformat.cosmosis_mean2values(mean_out, means_file , valuesfile)

	return

if __name__ == '__main__':
	main()